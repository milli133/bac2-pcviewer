import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  ngOnInit(): void {
    const worker = new Worker(new URL('./test.worker.ts', import.meta.url));

    if (typeof Worker !== 'undefined') {
      // Create a new
      worker.onmessage = ({ data }) => {
        console.log(`Service workers are working!`);
      };
      worker.postMessage('hello');
    } else {
      throw new Error('Service worker not supported in this browser!');
    }
  }

  title = 'Pointcloud Viewer';
}
