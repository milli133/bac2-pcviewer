import {
  AfterViewInit,
  Component,
  ElementRef,
  NgZone,
  ViewChild,
} from '@angular/core';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import {
  AmbientLight,
  Box3,
  BufferAttribute,
  BufferGeometry,
  Color,
  Float32BufferAttribute,
  MathUtils,
  PerspectiveCamera,
  Points,
  PointsMaterial,
  Scene,
  Vector3,
  WebGLRenderer,
} from 'three';
import { FileService } from 'src/app/services/File.service';
import { LASData } from 'src/app/interface/LASData.interface';
import { ParsedLASData } from 'src/app/interface/ParsedLASData.interface';
import * as Stats from 'stats-js';

@Component({
  selector: 'app-pcdviewer',
  templateUrl: './Viewer.component.html',
  styleUrls: ['./Viewer.component.scss'],
})
export class PCDViewerThreeComponent implements AfterViewInit {
  private renderer?: WebGLRenderer;
  private camera?: PerspectiveCamera;
  private scene?: Scene;
  private light?: AmbientLight;
  private controls?: OrbitControls;
  private stats = new Stats();

  private frameId?: number = 0;
  public voxelSize = 0.1;
  public visibleReload = false;
  public selectedFile = null;
  public loadEvent = new Event('newFile');

  private parsedLASData: ParsedLASData = {} as ParsedLASData;
  private parsedLASDataAny: LASData = {} as any;

  @ViewChild('canvas') canvas?: ElementRef<HTMLCanvasElement>;
  @ViewChild('canvasContainer') canvasContainer?: ElementRef<HTMLDivElement>;
  @ViewChild('voxelSizeInput') voxelSizeInput: ElementRef;

  public constructor(
    private ngZone: NgZone,
    private fileService: FileService
  ) {

    (function(){var script=document.createElement('script');script.onload=function(){var stats=new Stats();var statsEl = document.body.appendChild(stats.dom);statsEl.style="position: absolute; top:100px; left: 3px"; requestAnimationFrame(function loop(){stats.update();requestAnimationFrame(loop)});};script.src='https://mrdoob.github.io/stats.js/build/stats.min.js';document.head.appendChild(script);})()
  }

  ngAfterViewInit(): void {
    this.createScene(this.canvas!);
    this.render();
    this.animate();
    this.resize();
  }

  ngOnInit(): void {
    this.fileService.getCurrentData().subscribe({
      next: (data) => {
        (this.parsedLASData = data),
          this.addPointsFromParsedFile(
            this.makePointsWithBufferGeometry(
              this.convertParsedLASDataToBufferAttribute(data)
            )
          );
      },
    });

    this.fileService.getCurrentDataAny().subscribe({
      next: (data) => (this.parsedLASDataAny = data),

      error: (err) => console.error(err),
    });
  }

  public ngOnDestroy(): void {
    if (this.frameId != null) {
      cancelAnimationFrame(this.frameId);
    }
    if (this.renderer != null) {
      this.renderer.dispose();
      this.canvas!.nativeElement.innerHTML = '';
    }
  }

  public onReload() {
    this.visibleReload = false;
    this.loadEvent = new Event('reload');
    this.onFileSelected().then(() => {
      this.loadEvent = new Event('newFile');
    });
  }

  public onVoxelSizeChanged(): void {
    if (this.pointCloudLoaded()) this.visibleReload = true;
    this.fileService.setVoxelSize(Number(this.voxelSize));
  }

  public async loadSampleFile() {
    this.selectedFile = await fetch('assets/Zaghetto.pcd');

    this.selectedFile.arrayBuffer().then((ab) => {
      const points: Points = this.fileService.loadPCDFileLocally(ab);
      this.addPointsFromParsedFile(points);
    });
  }

  public async onFileSelected(event?: Event): Promise<any> {
    return new Promise((resolve, reject) => {
      if (!(this.loadEvent.type === 'reload'))
        this.selectedFile = (event.target as HTMLInputElement).files![0];

      // Get the file extension
      var extension = this.selectedFile.name.split('.').pop();

      switch (extension) {
        case 'pcd':
          this.selectedFile.arrayBuffer().then((ab) => {
            const points: Points = this.fileService.loadPCDFileLocally(ab);
            this.addPointsFromParsedFile(points);
            return Promise.resolve();
          });
          break;

        case 'las':
          this.selectedFile.arrayBuffer().then(async (ab) => {
            this.fileService.loadLasFileLocally(ab).then(() => {
              return Promise.resolve();
            });
          });
          break;
        case 'laz':
          this.fileService.showSnackMessage('LAZ not (yet) supported!');
          this.selectedFile = null;
          return Promise.reject('LAZ not (yet) supported!');
          break;
        default:
          this.fileService.showSnackMessage('Unsupported file type!');
          this.selectedFile = null;
          return Promise.reject('Unsupported file type!');
          break;
      }
      return Promise.resolve();
    });
  }

  public convertParsedLASDataToBufferAttribute(
    parsedLASData: ParsedLASData
  ): BufferGeometry {
    const positionAttribute = new BufferAttribute(
      new Float32Array(parsedLASData.position),
      3
    );

    const geometry = new BufferGeometry();
    geometry.setAttribute('position', positionAttribute);

    console.warn(geometry);

    return geometry;
  }

  public convertUint8ArrayToBufferGeometry(
    uint8Array: Uint8Array
  ): BufferGeometry {
    const length = uint8Array.length / 3;
    const positions = new Float32Array(length * 3);

    for (let i = 0; i < length; i++) {
      positions[i * 3] = uint8Array[i * 3];
      positions[i * 3 + 1] = uint8Array[i * 3 + 1];
      positions[i * 3 + 2] = uint8Array[i * 3 + 2];
    }

    const geometry = new BufferGeometry();
    geometry.setAttribute(
      'position',
      new BufferAttribute(new Float32Array(positions), 3)
    );

    console.warn(geometry);

    return geometry;
  }

  public makePointsWithBufferGeometry(geometry: BufferGeometry): Points {
    const material = new PointsMaterial({
      vertexColors: true,
      size: 0.01,
    });

    const pointsSample = new Points(geometry, material);
    pointsSample.geometry.rotateX(Math.PI);
    pointsSample.geometry.center();

    this.scene!.add(pointsSample);

    return pointsSample;
  }

  public addSamplePointsToScene() {
    const vertices = [];

    for (let i = 0; i < 20000; i++) {
      const x = MathUtils.randFloatSpread(10);
      const y = MathUtils.randFloatSpread(10);
      const z = MathUtils.randFloatSpread(10);

      vertices.push(x, y, z);
    }

    const geometry = new BufferGeometry();
    geometry.setAttribute('position', new Float32BufferAttribute(vertices, 3));

    const material = new PointsMaterial({
      color: 0x000000,
      fog: true,
      sizeAttenuation: true,
      size: 0.01,
    });

    const pointsSample = new Points(geometry, material);

    pointsSample.geometry.rotateX(Math.PI);
    pointsSample.geometry.center();

    this.scene!.add(pointsSample);

    console.log(pointsSample);
  }

  addPointsFromParsedFile(points: Points) {
    this.filterPointCloud();

    const geometry = points.geometry;
    const material = new PointsMaterial({
      color: 0x00,
      size: 0.0009,
      // sizeAttenuation: true,
    });
    geometry.rotateX(Math.PI);
    geometry.center();

    if (!(this.loadEvent?.type === 'reload'))
      this.centerCameraToFitContent(geometry);

    var points: Points = new Points(geometry, material);
    points.name = 'points';
    this.scene!.add(points);
  }

  centerCameraToFitContent(geometry: BufferGeometry) {
    geometry.computeBoundingSphere();

    const boundingBox: Box3 = geometry.boundingBox;

    var center = new Vector3();
    center = boundingBox.getCenter(center);

    var size = new Vector3();
    size = boundingBox.getSize(size);

    const fov = this.camera.fov * (Math.PI / 180);
    const maxDim = Math.max(size.x, size.y, size.z);
    let cameraZ = Math.abs((maxDim / 4) * Math.tan(fov * 2) * 3);

    this.camera.position.z = cameraZ;
    this.camera.position.y = center.y * 2;
    this.camera.position.x = center.x;
    this.camera.updateProjectionMatrix();

    const minZ = boundingBox.min.z;
    const cameraToFarEdge = minZ < 0 ? -minZ + cameraZ : cameraZ - minZ;

    this.camera.far = cameraToFarEdge * 5;
    this.camera.updateProjectionMatrix();

    this.controls.target = center;
    this.controls.maxDistance = cameraToFarEdge * 3;

    this.camera.lookAt(center);
  }

  filterPointCloud() {
    // delete all points from the scene
    this.scene.children = this.scene.children.filter(
      (e) => !(e instanceof Points)
    );
  }

  pointCloudLoaded(): boolean {
    var pcLoaded = false;

    this.scene.children.forEach((child) => {
      if (child instanceof Points) pcLoaded = true;
    });

    return pcLoaded;
  }

  public createScene(canvas: ElementRef<HTMLCanvasElement>): void {
    this.renderer = new WebGLRenderer({
      canvas: this.canvas!.nativeElement,
      alpha: false, // transparent background
      antialias: true, // smooth edges,
      preserveDrawingBuffer: false,
      powerPreference: 'high-performance',
      stencil: false,
    });
    this.renderer.setSize(100, 100);

    // create the scene
    this.scene = new Scene();
    this.scene.background = new Color(0xffffff);

    this.camera = new PerspectiveCamera(
      30,
      window.innerWidth / window.innerHeight,
      0.01,
      99999
    );
    // this.camera.position.set(1, 0, 1);
    this.scene.add(this.camera);

    this.controls = new OrbitControls(this.camera, this.canvas?.nativeElement);
    this.controls.minDistance = 0.01;
    this.controls.maxDistance = 50000;

    // soft white light
    this.light = new AmbientLight(0x404040);
    this.light.position.z = 1;
    this.scene.add(this.light);

    // stats
    // this.stats = new Stats();
    // this.stats.showPanel(0);
    // this.canvasContainer.nativeElement.appendChild(this.stats.dom); // <-- remove me
  }

  public animate(): void {      this.stats.update();

    // We have to run this outside angular zones,
    // because it could trigger heavy changeDetection cycles.
    this.ngZone.runOutsideAngular(() => {
      if (document.readyState !== 'loading') {
        this.render();
      } else {
        window.addEventListener('DOMContentLoaded', () => {
          this.render();
        });
      }

      window.addEventListener('resize', () => {
        this.resize();
      });
    });
  }

  public render(): void {
    this.frameId = requestAnimationFrame(() => {

      this.render();
    });

    this.renderer!.render(this.scene!, this.camera!);
  }

  public resize(): void {
    const width = this.canvasContainer!.nativeElement.offsetWidth;
    const height = this.canvasContainer!.nativeElement.offsetHeight;

    this.camera!.aspect = width / height;
    this.camera!.updateProjectionMatrix();

    this.renderer!.setSize(width, height);
  }
}
