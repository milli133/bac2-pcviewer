# Pcviewer

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 15.2.7.

## Deployed on AWS Amplify

Visit this link for a deployed version of the app: https://prod.d27jcy60imfv6o.amplifyapp.com/#/viewer

## Code source

Gitlab: https://gitlab.com/milli133/bac2-pcviewer

Point clouds for testing are located in the project src/pointclouds folder!

## Development server

Run `npm run start` and open http://localhost:4200/

## Build

Build with `npm run build` and open the index.html file in a live server or host on server
