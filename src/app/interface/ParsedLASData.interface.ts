export interface ParsedLASData {
  mean: { 0: number; 1: number; 2: number };
  position: Float32Array;
  color: Uint8Array;
  intensity: Float32Array;
  classification: Uint8Array;
  returnNumber: Uint8Array;
  numberOfReturns: Uint16Array;
  pointSourceID: Uint16Array;
  tightBoundingBox: {
    min: { 0: number; 1: number; 2: number };
    max: { 0: number; 1: number; 2: number };
  };
  indices: Uint16Array;
}
