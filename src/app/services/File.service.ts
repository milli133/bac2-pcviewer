import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Observable } from 'rxjs';
import { Points } from 'three';
import { ParsedLASData } from '../interface/ParsedLASData.interface.js';
import { LASFile, LASDecoder } from './worker/laslaz.js';
import { PCDLoader } from 'three/examples/jsm/loaders/PCDLoader.js';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root',
})
export class FileService {
  private dataSubject: Subject<ParsedLASData>;
  private dataSubjectAny: Subject<any>;
  public voxelSize = 0.01;

  constructor(private snackBar: MatSnackBar) {
    this.dataSubject = new Subject<ParsedLASData>();
    this.dataSubjectAny = new Subject<any>();
  }

  getCurrentData(): Observable<ParsedLASData> {
    return this.dataSubject.asObservable();
  }

  getCurrentDataAny(): Observable<any> {
    return this.dataSubjectAny.asObservable();
  }

  setVoxelSize(voxelSize: number) {
    this.voxelSize = voxelSize;
  }

  showSnackMessage(message: string, action: string = 'x') {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }

  async loadLasFileLocally(data: ArrayBuffer): Promise<any> {
    const arrayBuffer = new Uint8Array(data);

    try {
      const laszip = new LASFile(arrayBuffer.buffer);
      this.showSnackMessage('Loading LAS File');

      laszip.open();
      laszip.setVoxelSize(this.voxelSize);
      const readHeader = await laszip.getHeader();
      // laszip.read();
      const readData = await laszip.readData(data.byteLength, 0, 5);
      const decoder = new LASDecoder(
        readData.buffer,
        readData.buffer.byteLength,
        readHeader
      );
      const parsedData = await decoder.decodeLas(this.voxelSize);
      this.dataSubject.next(parsedData);
      this.showSnackMessage('LAS File loaded!');
    } catch (error) {
      console.error(error);
      this.dataSubject.next(null);
    }
  }

  public loadPCDFileLocally(data: ArrayBuffer): Points | null {
    this.showSnackMessage('Loading PCD File');

    const loader = new PCDLoader();
    try {
      const parsedPCD = loader.parse(data);
      if (parsedPCD != null) {
        this.showSnackMessage('PCD File loaded!');
        return parsedPCD;
      }
    } catch (error) {
      this.showSnackMessage(error);
    }

    return null;
  }
}
