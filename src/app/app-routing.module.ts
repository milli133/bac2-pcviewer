import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PCDViewerThreeComponent } from './components/Viewer/Viewer.component';

const routes: Routes = [
  { path: 'viewer', component: PCDViewerThreeComponent },
  { path: '', redirectTo: '/viewer', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule],
})
export class AppRoutingModule {}
