export interface LASData {
  buffer: ArrayBuffer;
  count: number;
  hasMoreData: boolean;
  header: any;
}
